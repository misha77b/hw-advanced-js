//                                Задание 7
console.log('Задание 7');

const array = ['value', () => 'showValue'];

const [value, showValue] = array;

// console.log(value, showValue);
alert(value); // должно быть выведено 'value'
alert(showValue()); // должно быть выведено 'showValue'