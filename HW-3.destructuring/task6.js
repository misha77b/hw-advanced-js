//                                Задание 6
console.log('Задание 6');

const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}

const newEmployee = {
    ...employee,
    age: 48,
    salary: '5475$'
};

console.log(newEmployee);