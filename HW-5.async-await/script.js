//                                               Теоретический вопрос
// 1. Обьясните своими словами, как вы понимаете асинхронность в Javascript
/*Асинхонность в JS это возможность не останавливаться на месте, в котором идет выполнение какого-либо кода, а начинать выполнять другую функцию паралельно, пока предыдущее действие завершается.
*/

const ipBtn = document.querySelector('.ip-btn');
const root = document.querySelector('#root');

class GetInfo {
    constructor (ip) {
        this.ip = ip;
    }
    async getIp() {
        const res = await fetch('https://api.ipify.org/?format=json')
        .then(res => res.json());
        const ip = res.ip;
        console.log(ip);
        this.getLocation(ip);
    }

    async getLocation(ip) {
        const locationByIp = await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,region,city,district`)
        .then(res => res.json());
        console.log(locationByIp);
        this.render(locationByIp);
    }

    render(locationByIp) {
        root.innerHTML = `
        <p class="info">Your continent is <b>${locationByIp.continent}</b> <br>
        You live in <b>${locationByIp.country}</b> <br>
        Region <b>${locationByIp.region}</b> <br>
        In <b>${locationByIp.city}</b> city <br> 
        On disrict ${locationByIp.district}</p>`
    }
}

const getInfo = new GetInfo();
ipBtn.addEventListener('click', () => {
    getInfo.getIp();
})