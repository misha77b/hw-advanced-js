//                                             Теоретический вопрос
// 1. Приведите пару примеров, когда уместно использовать в коде конструкцию try...catch.
/*  Try...catch уместно использовать тогда, когда мы предполагаем, что в этой части кода может произойти ошибка, и чтобы не прерывать выполнение, мы оборачиваем эту часть в try...catch. Например, когда мы хотим обратиться к вложеному объекту типа user.name, а user.name === undefined, тогда мы оборачиавем эту чатсь кода в конструкцию try...catch, чтобы не прерывать работу кода.
 */

const books = [{
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const root = document.querySelector('#root');
const list = document.createElement('ul');
root.appendChild(list);

const booksList = books.map(function(item) {
    try {
        if (!item.author) {
            throw new SyntaxError('Information about author does not exist');
        } else if (!item.name) {
            throw new SyntaxError('Information about name does not exist');
        } else if (!item.price) {
            throw new SyntaxError('Information about price does not exist');
        } else {
            return `<li> Written by: ${item.author} <br> Book name: ${item.name} <br> Price: ${item.price} </li>`
        }
    } catch (err) {
        console.log(err);
    }

    console.log(item);
})

list.insertAdjacentHTML('beforeend', `${booksList.join(' ')}`);