//                                       Теоретический вопрос
// 1.Обьясните своими словами, что такое AJAX и чем он полезен при разработке на Javascript.
/*  AJAX это набор методов веб-разработки, который позволяет нам получать данные с сервера и отправлять их на него без перезарузки страницы. AJAX упрощает многозадачность, т.е. позволяет нам одновременно выполнять несколько операций, тем самым делает удобной работу пользователю, не заставляя его долго ждать загрузку контента.
 */


const url = 'https://ajax.test-danit.com/api/swapi/films';
const root = document.querySelector('.root');

class Films {
    constructor(films) {
        this.films = films;
    }

    async getFilms() {
        this.films = await fetch(url)
            .then(response => response.json());
        this.getCharacters(this.films);
        this.renderFilms(this.films);
    }

    async getCharacters(films) {
        await films.forEach(film => {
            film.characters.forEach(async function(link) {
                const character = await fetch(`${link}`)
                    .then(response => response.json())
                    .then(result => result.name)

                Films.renderCharacters(character, film.id);
            })
        })
    }

    renderFilms(films) {
        const list = document.createElement('ul');
        root.appendChild(list);
        films.forEach(film => {
            const li = document.createElement('li');
            li.id = `${film.id}`
            li.innerHTML = `${film.episodeId} <br> ${film.name} <br> ${film.openingCrawl}`
            const charactersName = document.createElement('p');
            charactersName.classList.add('character');
            li.appendChild(charactersName);
            list.appendChild(li);
        })
    }

    static renderCharacters(character, filmId) {
        const filmBlock = document.getElementById(`${filmId}`);
        const characterName = filmBlock.querySelector('.character')
        filmBlock.appendChild(characterName);
        characterName.innerHTML += `${character} <br>`;
    }
}

const allFilms = new Films();
allFilms.getFilms();