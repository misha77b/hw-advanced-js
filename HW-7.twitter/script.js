const usersUrl = 'https://ajax.test-danit.com/api/json/users';
const postsUrl = 'https://ajax.test-danit.com/api/json/posts';
const root = document.querySelector('#root');

document.addEventListener('DOMContentLoaded', async function() {
    const usersDataFetch = await fetch(usersUrl).then(response => response.json());
    const userList = usersDataFetch.map(user => new User(user))
    const postsDataFetch = await fetch(postsUrl).then(response => response.json());
    const postList = postsDataFetch.map(post => new Post({...post, user: userList.find(user => user.id === post.userId) }));

    postList.map(post => new Card(post).render());
})

class User {
    constructor({name, email, id}) {
            this.name = name;
            this.email = email;
            this.id = id;
        }
}

class Post {
    constructor({ id, title, body, user}) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.user = user;
    }
}

class Card {
    constructor({id, title, body, user}) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.user = user;
    }

    render() {
        const post = document.createElement('div');
        post.dataset.id = this.id;
        post.classList.add('post-block');
        post.innerHTML = `
        <span class="post-user-name">${this.user.name}</span>
        <span class="post-user-email">${this.user.email}</span>
        <div class="post-img"> Here could be post img </div>
        <h2 class='post-title'>${this.title} </h2>
        <p class='post-body'>${this.body}</p>
        <button class="delete-post-btn">Delete Post</button>`;
        root.appendChild(post);

        post.addEventListener('click', async function (event)  {
            let deleteBtn = event.target.classList == 'delete-post-btn';
            let postId = event.target.parentElement.dataset.id;
            if(deleteBtn) {
                await fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
                    method: 'DELETE'
                });
                setTimeout(() => {
                    post.remove();
                    console.log(`Post number ${postId} is removed`);
                }, 1000)
            }
        })
    }
}