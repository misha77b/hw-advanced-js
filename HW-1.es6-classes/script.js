//                                           Теоретический вопрос
//1. Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript.
/* Прототипное наследование используется в тех случаях, когда у нас есть один объект со своими свойствами и методами, и мы хотим создать еще несколько объектов, которые будут идентичны, за исключением некоторых параметров.
В таких случаях  мы наследуемся от перовго объетка, чтобы не копировать и не переопределять его методы для других, а просто, на его базе, создать новый, в следствии чего получаем несколько идентичных объектов, в которые можем записывать разные свойства и параметры, + те объекты, которые принимают наследование могут быть расширены другими, нужными для них свойствами и методами.
 */

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name() {
        return this._name;
    }
    set name(value) {
        return this._name = value
    }

    get age() {
        return this._age;
    }
    set age(value) {
        return this._age = value
    }

    get salary() {
        return this._salary;
    }
    set salary(value) {
        return this._salary = value
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.salary = salary;
        this._lang = lang;
    }

    get salary() {
        return this._salary;
    }
    set salary(value) {
        this._salary = value * 3;
    }
}

// let firstEmployee = new Employee('Kirill', 25, 150);
let firstProgrammer = new Programmer('Misha', 19, 350, 'French, German, English');
let secondProgrammer = new Programmer('Anton', 25, 750, 'German, French, English');
let thirdProgrammer = new Programmer('Kristina', 28, 500, 'English, French, German');

// console.log(firstEmployee);
console.log(firstProgrammer);
console.log(secondProgrammer);
console.log(thirdProgrammer);